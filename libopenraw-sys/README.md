Regenerating the bindings

* Set the environment `OR_PATH` to were the libopenraw source tree is
(top-level). The headers will be looked up in `$OR_PATH/libopenraw`.
* Make sure bindgen is in your `PATH`
* Run `generate_bindings.sh`

