#[allow(non_camel_case_types)]
pub mod ffi_libopenraw;

pub use ffi_libopenraw::*;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
