fn main() {
    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=wrapper.h");

    if std::env::var("DOCS_RS").is_ok() {
        return;
    }

    // Detect libraries with pkg-config
    let libopenraw = pkg_config::Config::new().probe("libopenraw-0.3").unwrap();

    // Tell cargo to tell rustc to link the system libopenraw shared library
    for lib in &libopenraw.libs {
        println!("cargo:rustc-link-lib={}", lib);
    }
}
