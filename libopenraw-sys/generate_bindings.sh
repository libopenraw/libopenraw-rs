#!/bin/sh

if [ -z "$OR_PATH" ] ; then
    echo "set OR_PATH env to point to the top-level source tree of libopenraw"
    exit 2
fi

# Notes:
# __darwin_size_t keep being generated on macOS.
#
bindgen wrapper.h -o src/ffi_libopenraw.rs \
        --whitelist-function 'or_.*' \
        --whitelist-type 'or_.*' \
        --whitelist-type 'OR.*' \
        --whitelist-type '_OR_.*' \
        --blacklist-type __darwin_size_t \
        --rustified-enum 'or_.*' \
        --rustified-enum '_OR_TYPEID.*' \
        --rustified-enum 'Exif.*' \
        --rustified-enum '_debug_.*' \
        --size_t-is-usize \
        -- \
        -I$OR_PATH/include
