/*
 * libopenraw-rs
 *
 * Copyright (C) 2021-2022 Hubert Figuière
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use super::DataType;
use super::Result;

use libopenraw_sys as ffi;

/// Thumbnail
pub struct Thumbnail(pub(crate) ffi::ORThumbnailRef);

impl Thumbnail {
    /// Create a new Thumbnail
    pub fn new() -> Self {
        Self(unsafe { ffi::or_thumbnail_new() })
    }

    /// Get the data size
    pub fn get_data_size(&self) -> usize {
        unsafe { ffi::or_thumbnail_data_size(self.0) }
    }

    /// Get the thumbnail data.
    pub fn get_data(&self) -> Result<&[u8]> {
        let d = unsafe { ffi::or_thumbnail_data(self.0) as *const u8 };
        if d.is_null() {
            return Err(super::Error::NotFound);
        }
        let size = self.get_data_size();
        Ok(unsafe { std::slice::from_raw_parts(d, size) })
    }

    /// Get the thumbnail format
    pub fn get_format(&self) -> DataType {
        unsafe { ffi::or_thumbnail_format(self.0) }.into()
    }

    /// Get the (x, y) dimension of the thumbnail
    pub fn get_dimensions(&self) -> (u32, u32) {
        let mut x = 0u32;
        let mut y = 0u32;
        unsafe { ffi::or_thumbnail_dimensions(self.0, &mut x, &mut y) };
        (x, y)
    }
}

impl Default for Thumbnail {
    fn default() -> Self {
        Self::new()
    }
}

impl Drop for Thumbnail {
    /// Automatically release the Thumbnail
    fn drop(&mut self) {
        unsafe { ffi::or_thumbnail_release(self.0) };
    }
}
