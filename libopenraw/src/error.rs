/*
 * libopenraw-rs
 *
 * Copyright (C) 2021-2022 Hubert Figuière
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use libopenraw_sys as ffi;

#[repr(u32)]
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
/// Error type (mapping to the C error types).
///
/// While the values match the C implementaton, do expect this to be
/// the case.
pub enum Error {
    /// No error
    None = ffi::or_error::OR_ERROR_NONE as u32,
    /// Buffer is too small.
    BufTooSmall = ffi::or_error::OR_ERROR_BUF_TOO_SMALL as u32,
    /// Invalid ref / pointer.
    NotARef = ffi::or_error::OR_ERROR_NOTAREF as u32,
    /// Can't open file.
    CantOpen = ffi::or_error::OR_ERROR_CANT_OPEN as u32,
    /// Stream was closed.
    ClosedStream = ffi::or_error::OR_ERROR_CLOSED_STREAM as u32,
    /// Whatever you were looking for wasn't found.
    NotFound = ffi::or_error::OR_ERROR_NOT_FOUND as u32,
    /// Invalid parameter.
    InvalidParam = ffi::or_error::OR_ERROR_INVALID_PARAM as u32,
    /// Invalid format.
    InvalidFormat = ffi::or_error::OR_ERROR_INVALID_FORMAT as u32,
    /// Decompression format.
    Decompression = ffi::or_error::OR_ERROR_DECOMPRESSION as u32,
    /// Not implemented.
    NotImpleemented = ffi::or_error::OR_ERROR_NOT_IMPLEMENTED as u32,
    /// Already open.
    AlreadyOpen = ffi::or_error::OR_ERROR_ALREADY_OPEN as u32,
    /// Unknown.
    Unknown = ffi::or_error::OR_ERROR_UNKNOWN as u32,
}

impl std::convert::From<u32> for Error {
    fn from(e: u32) -> Error {
        if e > ffi::or_error::OR_ERROR_ALREADY_OPEN as u32 {
            Error::Unknown
        } else {
            unsafe { std::mem::transmute(e) }
        }
    }
}

impl std::convert::From<ffi::or_error> for Error {
    fn from(e: ffi::or_error) -> Error {
        (e as u32).into()
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
}
