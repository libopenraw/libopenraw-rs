/*
 * libopenraw-rs
 *
 * Copyright (C) 2021 Hubert Figuière
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use std::ffi::CString;

pub(crate) fn path_to_filename<P: AsRef<std::path::Path>>(filename: P) -> CString {
    // XXX will break on Windows
    use std::os::unix::ffi::OsStrExt;
    let p = filename.as_ref().as_os_str();
    CString::new(p.as_bytes()).unwrap()
}
