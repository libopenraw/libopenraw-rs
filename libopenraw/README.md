libopenraw
==========

This is the Rust API for libopenraw.

It needs libopenraw 0.3 by way of `libopenraw-sys`.

Its current implementation is limited to extrating thumbnails.


```Rust
use libopenraw as or;

let raw_file = or::RawFile::from_file(path, or::RawFileType::Unknown)
    .expect("Couldn't open raw file");
let orientation = raw_file.get_orientation();
let thumbnail = raw_file.get_thubmnail(160).expect("Couldn't find thumbnail");
// Get the size fo the thumbnail
let (x, y) = thumbnail.get_dimensions();
let buf = thumbnail.get_data().expect("Couldn't get the data");
```
