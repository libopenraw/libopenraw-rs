libopenraw-rs
=============

Rust bindings for libopenraw.

Requires libopenraw 0.3.x.

`libopenraw-sys` is the direct bindings generated with bingen.
`libopenraw` is the Rust API.
